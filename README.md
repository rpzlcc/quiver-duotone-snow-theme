# quiver-duotone-snow-theme

## Quiver app Duotone Snow theme

This is a theme file for Quiver <http://happenapps.com/#quiver>.

It's based on the Duotone Snow theme within [Bear writing app](http://www.bear-writer.com/)

![Duotone Snow screenshot](https://raw.githubusercontent.com/rpzlcc/quiver-duotone-snow-theme/master/screenshot.png)

---

## Credits

### Lato font

Copyright (c) 2010-2014, Łukasz Dziedzic (dziedzic@typoland.com), with Reserved Font Name Lato
*Sourced from <https://www.fontsquirrel.com/fonts/lato>*

### Red Graphite Theme

<https://github.com/floq-design/quiver-red-graphite-theme>